<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'proinnova' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'BhvLDo)K>RdgeHd.b+]GY2w*8tlc>^iI{3dfEIB17KNbd2N;Qj1t4|8pqRUmUcUj' );
define( 'SECURE_AUTH_KEY',  '_JK*>CrbE=GvNfYlK7^IgZ}D4!.J/{vpEM`pmzzybJq=L-0;sv6YnR l4=#&S2OV' );
define( 'LOGGED_IN_KEY',    '&_Lkd-tCjb<4j(/>gv@UojWCmMm!e3tjU(+Zp8Ez&NwYO32Su?qcb+Fok($Q[H8a' );
define( 'NONCE_KEY',        'b6bj7)W|]13-X,V{QoWORxx[p:j!{s-A!yGtS -RMN/}(2jB2sM~&HG>D[S7$Kxe' );
define( 'AUTH_SALT',        '*o_k13{G=3$erT&[A!9Q/_CUM7(E,EKi*9^W]+SI`-i8EXYi<T )9f79/#CJGz17' );
define( 'SECURE_AUTH_SALT', 'cT*~/.>=4jmRe3 q++=c:@?a9.1fsP0tM@;XX-xp8*rb%f{l^qRG.Ww<SOy~6GIr' );
define( 'LOGGED_IN_SALT',   'Nl4d1LQvroGc+[zZGg Ol0lq6C7se5z=<kKY/S4!T^*u_[?5+3A*}Vw|Kg/X%p,P' );
define( 'NONCE_SALT',       '(OSn1gfN)$^fq.d]^l$wrV:JV,<Bw#+/<taO^[qgZ{mDDeGA4(B`O yP7|HkTU2W' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
