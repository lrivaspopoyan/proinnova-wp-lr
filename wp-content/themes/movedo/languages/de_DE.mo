��    2      �  C   <      H     I     V  
   c     n     r     {          �     �     �     �     �     �     �     �     �     �          &     .     F     S     W  	   j     t     |     �     �  
   �     �     �     �     �     �  O   
  E   Z     �     �     �     �     �     �     �  $   �            	      	   *     4  �  G     
     
     (
     7
     <
     C
     H
     _
     q
     �
     �
     �
  )   �
  
   �
     �
     �
     �
          /  #   8     \     t     x  
   �  	   �     �     �  !   �     �     �     �                $  ^   9  d   �     �                )     0     ?     U  $   f     �     �     �     �      �                                  %               1   .   -            $             #          *          !              
      ,             )               	          0   /   "         &              2             '   +         (          %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Home Hours Leave a Reply Leave a Reply to Link Relationship (XFN) Log out Log out of this account Logged in as Min Monthly Archives : Move down Move up Name Navigation Label Open link in a new window/tab Posts By : Search Results for : Search Results for : %s Search for ... Sec Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. Title Attribute URL Website Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. at in logged in read more to post a comment. Project-Id-Version: Movedo v3.0.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-06 22:39+0300
PO-Revision-Date: 2019-05-06 22:39+0300
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
 %s (ungültig) %s (Entwurf) (erforderlich) Alle Archiv Von: CSS-Klassen (optional) Antwort abbrechen Kommentare sind geschlossen. Tagesarchiv : Tage E-Mail Modifier l&rsquo;élément de menu&nbsp;: Startseite Stunden Hinterlasse eine Antwort Hinterlasse eine Antwort zu Link-Beziehung (XFN) Abmelden Melden Sie sich von diesem Konto ab Sie sind angemeldet als Min Monatsarchiv : Nach unten Nach oben Name Angezeigter Name Link in neuem Fenster/Tab öffnen Beiträge Von : Suchergebnisse für : Suchergebnisse für: %s Suche nach... Sek Kommentar abschicken Die Beschreibung wird im Menü angezeigt, sofern das aktive Theme diese Funktion unterstützt. Dieser Beitrag ist passwortgeschützt. Bitte geben Sie das Passwort ein, um die Kommentare zu sehen. HTML-Attribut title (optional) URL Webseite Wochen Jahresarchiv : Sie müssen... (sein) Ihr Kommentar... Ihr Kommentar wartet auf Moderation. um in angemeldet sein weiterlesen um einen Kommentar zu schreiben. 