��    +      t  ;   �      �     �     �  
   �     �     �     �     �               (     9     >     E     T     Y     _     m     ~     �     �     �  	   �     �     �     �  
   �     �          "  O   1  E   �     �     �     �     �     �  $        -     0  	   3  	   =     G  �  Z     	     0	     D	     [	     b	     o	  '   w	  !   �	  >   �	      
     
     ,
  4   3
     h
     u
  $   ~
  +   �
     �
  @   �
     %     C     `     w  
   �  T   �     �  5        A     _  �   }  �   �     �     �     �       "     2   <     o     t  #   y     �  !   �                                                        '              !                               #   )   *                                      &   (   	         %             
   +       $      "        %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Home Hours Leave a Reply Leave a Reply to Log out Log out of this account Logged in as Monthly Archives : Move down Move up Name Open link in a new window/tab Posts By : Search Results for : Search for ... Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. Website Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. at in logged in read more to post a comment. Project-Id-Version: Movedo v3.0.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-06 22:39+0300
PO-Revision-Date: 2019-05-06 22:39+0300
Last-Translator: 
Language-Team: 
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
 %s (Μη έγκυρο) %s (Αναμονή) (απαιτείται) Όλα Αρχεία Από: Κλάση CSS (προαιρετικό) Ακύρωση απάντησης Τα σχόλια είναι απενεργοποιημένα. Ημερήσια αρχεία: Ημέρες E-mail Επεξεργασία Στοιχείου Μενού Αρχική Ώρες Γράψτε μια απάντηση Γράψτε μια απάντηση στο Αποσύνδεση Αποσύνδεση από αυτόν το λογαριασμό Συνδεδεμένος ως Μηνιαία αρχεία: Προς τα κάτω Προς τα πάνω Όνομα Άνοιγμα του συνδέσμου σε νέο παράθυρο/καρτέλα Αναρτήσεις από: Αποτελέσματα αναζήτησης για: Αναζήτηση για ... Υποβολή σχολίου Η περιγραφή θα εμφανιστεί στο μενού, εάν το τρέχων θέμα το υποστηρίζει. Αυτή η ανάρτηση προστατεύεται με κωδικό πρόσβασης. Εισάγετε τον κωδικό πρόσβασης για να δείτε τα σχόλια. Ιστοσελίδα Εβδομάδες Ετήσια Αρχεία: Θα πρέπει να Το σχόλιό σας εδώ... Το σχόλιο αναμένει έγκριση. σε σε είστε συνδεδεμένος περισσότερα για να σχολιάσετε. 