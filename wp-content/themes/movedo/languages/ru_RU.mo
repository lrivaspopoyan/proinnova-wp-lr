��    -      �  =   �      �     �     �  
   �          
               .     ;     P     a     f     m     |     �     �     �     �     �     �     �  	   �     �     �     �  
        &     ,     A     P  O   _  E   �     �     �                    %  $   :     _     b  	   e  	   o     y  �  �     N	     b	  #   {	     �	     �	     �	  )   �	     �	  &   	
     0
     L
     U
  #   \
     �
  
   �
     �
  *   �
  
   �
  8   �
       !   1     S  
   \     g  A   n     �     �  )   �     �  )     �   0  �   �     8     E     N     [  
   u  5   �  /   �     �     �  
   �     �  3        $   
             	              #          (         %       &   !       )                           "                                             *            +          '         -                   ,          %s (Invalid) %s (Pending) (required) All Archives By: CSS Classes (optional) Cancel Reply Comments are closed. Daily Archives : Days E-mail Edit Menu Item Home Hours Leave a Reply Leave a Reply to Log out Log out of this account Logged in as Monthly Archives : Move down Move up Name Open link in a new window/tab Posts By : Reply Search Results for : Search for ... Submit Comment The description will be displayed in the menu if the current theme supports it. This post is password protected. Enter the password to view comments. URL Website Weeks Yearly Archives : You must be Your Comment Here... Your comment is awaiting moderation. at in logged in read more to post a comment. Project-Id-Version: Movedo v3.0.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-05-06 22:40+0300
PO-Revision-Date: 2019-05-06 22:40+0300
Last-Translator: 
Language-Team: 
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: woocommerce
 %s (неверно) %s (в ожидании) (обязательное поле) Все Архивы Подпись: CSS классы (опционально) Отменить ответ Комментарии закрыты. Архивы по дням: Дней E-mail Редактировать меню Главная Часов Ответить Оставить комментарий к Выйти Выйдите из этой учетной записи Вы вошли как Архивы по месяцам: Вниз Вверх Имя Открыть ссылку в новом окне/вкладке Записи: Ответить Результаты поиска для: Ищем ... Отправить комментарий Описание будет отображаться в меню, если текущая тема поддерживает его. Эта запись защищена паролем. Введите пароль, чтобы увидеть комментарии. Ссылка Сайт Недель Архивы за год: Нужно Оставьте комментарий здесь… Комментарий на модерации. в в войти читать для публикации комментария. 