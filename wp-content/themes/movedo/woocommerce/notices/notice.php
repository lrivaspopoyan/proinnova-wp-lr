<?php
/**
 * Show messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ){
	return;
}

?>

<?php foreach ( $messages as $message ) : ?>
	<div class="woocommerce-info grve-woo-info grve-bg-blue">
		<?php
			if ( function_exists( 'wc_kses_notice' ) ) {
				echo wc_kses_notice( $message );
			} else {
				echo wp_kses_post( $message );
			}
		?>	
	</div>
<?php endforeach;
	
//Omit closing PHP tag to avoid accidental whitespace output errors.
